`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:40:56 10/27/2016 
// Design Name: 
// Module Name:    reg 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module reg_48(clk, reset, in, out);
	
	input clk;
	input reset;
	input[47:0] in;
	output reg[47:0] out;

	always @(posedge clk or posedge reset)  
    begin  
      if (reset)  
        out = 0;  
      else  
        out = in;  
    end  

endmodule
