`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:47:53 10/27/2016
// Design Name:   reg_16
// Module Name:   T:/323CB/lab2/test_reg.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: reg_16
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_reg;

	// Inputs
	reg clk;
	reg reset;
	reg [15:0] in;

	// Outputs
	wire [15:0] out;

	// Instantiate the Unit Under Test (UUT)
	reg_16 uut (
		.clk(clk), 
		.reset(reset), 
		.in(in), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		
		reset = 0;
		in = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		in = 16'd1024;
		// Add stimulus here
		#10;
		in = 16'd2047;
		  
	end
	
	always begin
      #5  clk = ~clk; // Toggle clock every 5 ticks
	end
      
endmodule

