`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:23:50 11/14/2016
// Design Name:   pipe
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test_testmod1.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pipe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_testmod1;

	// Inputs
	reg clr;
	reg reset;
	reg clk;
	reg [31:0] op1;
	reg [31:0] op2;
	reg op;

	// Outputs
	wire [31:0] rez;

	// Instantiate the Unit Under Test (UUT)
	pipe uut (
		.clr(clr), 
		.reset(reset), 
		.clk(clk), 
		.op1(op1), 
		.op2(op2), 
		.op(op), 
		.rez(rez)
	);

	initial begin
		// Initialize Inputs
		clr = 0;
		reset = 0;
		clk = 0;
		op1 = 0;
		op2 = 0;
		op = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

