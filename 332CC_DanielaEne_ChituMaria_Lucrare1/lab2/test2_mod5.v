`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:04:23 11/19/2016
// Design Name:   mod5
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod5.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod5
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod5;

	// Inputs
	reg [47:0] mantise;
	reg op;
	reg semn;

	// Outputs
	wire [25:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod5 uut (
		.mantise(mantise), 
		.op(op), 
		.out(out), 
		.semn(semn)
	);

	initial begin
		// Initialize Inputs
		mantise = 0;
		op = 0;
		semn = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		op = 0;
		mantise = 48'b101010110011001100110011011001100110011001100110;
		semn = 1;
	end
      
endmodule

