`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:51:06 11/03/2016 
// Design Name: 
// Module Name:    mod4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod4(mantise, semn, out);
//exponentul cel mai mare + mantise concatenate
	input wire[56:0] mantise;
	output reg [47:0] out;
	output reg semn;
	
	reg[8:0] chosen;
	reg[23:0] m1;
	reg[23:0] m2;
	reg temp1;
	reg temp2;
	
	always @(mantise) begin

		chosen = mantise[56:48];
		m1 = mantise[47:24];
		m2 = mantise[23:0];
		temp1 = m1[23];
		temp2 = m2[23];
		m1[23] = 1'b1;
		m2[23] = 1'b1;
		//exponentul 1 e mai mare		
		if (chosen[8] == 1'b0)begin
		//shiftez m2 cu diferenta dintre exponenti
				m2 = m2 >> chosen[7:0];
				out = {m1, m2};
				semn = temp1;
		end
		else begin	
		//exponentul 2 e mai mare
		//shiftez m1 cu diferenta dintre exponenti
				m1 = m1 >> chosen[7:0];
				out = {m2, m1};
				semn = temp2;
			end
		
	end

endmodule
