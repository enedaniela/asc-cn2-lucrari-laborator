`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:56:16 12/19/2016
// Design Name:   pipe
// Module Name:   C:/Users/dana/Desktop/To clean/lab4/lab2/testAll.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pipe
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module testAll;

	// Inputs
	reg clr;
	reg reset;
	reg clk;
	reg [31:0] op1;
	reg [31:0] op2;
	reg op;

	// Outputs
	wire [23:0] rezmant;
	wire [7:0] rezexp;

	// Instantiate the Unit Under Test (UUT)
	pipe uut (
		.clr(clr), 
		.reset(reset), 
		.clk(clk), 
		.op1(op1), 
		.op2(op2), 
		.op(op), 
		.rezmant(rezmant), 
		.rezexp(rezexp)
	);

	initial begin
		// Initialize Inputs
		clr = 0;
		reset = 0;
		clk = 0;
		op1 = 0;
		op2 = 0;
		op = 0;

		// Wait 100 ns for global reset to finish
		op1 = 32'b10000010101010110011001100110011;//10.7
		op2 = 32'b10000001110011001100110011001100;//6.4
		#20
		op1 = 32'b10000010110001101011100001010001;//12.42
		op2 = 32'b10000010100101010111000010100011;//9.34
        
		// Add stimulus here

	end
      
	always begin
	
		 #10 clk = ~clk;
	
	end
endmodule

