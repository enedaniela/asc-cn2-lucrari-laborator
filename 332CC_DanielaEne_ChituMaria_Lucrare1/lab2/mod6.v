`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:21:45 11/03/2016 
// Design Name: 
// Module Name:    mod6 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod6(mantise, chosen, out);

	input [25:0] mantise;
	output reg[8:0] chosen;
	output reg[23:0] out;
	reg semn;
	reg[23:0] m;
	
	
	always @ (mantise) begin
		semn = mantise[25];
		m = mantise[24:1];
		chosen = 9'b0;
			if(mantise[24] == 1'b1) begin
				chosen[8] = 1'b1;
				chosen[7:0] = chosen[7:0] + 1'b1;
			end			
		out[22:0] = m[22:0];
		out[23] = semn;
	end
endmodule
