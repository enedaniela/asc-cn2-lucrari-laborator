`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:25:17 11/14/2016
// Design Name:   mod1
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod1.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod1
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod1;

	// Inputs
	reg [15:0] exps;

	// Outputs
	wire [8:0] chosen;
	wire [15:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod1 uut (
		.exps(exps), 
		.chosen(chosen), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		exps = 0;

		// Wait 100 ns for global reset to finish
		#100;
      exps = 16'b1000001010000001;
		// Add stimulus here

	end
      
endmodule

