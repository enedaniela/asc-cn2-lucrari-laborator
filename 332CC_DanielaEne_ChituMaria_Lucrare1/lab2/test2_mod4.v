`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:40:17 11/14/2016
// Design Name:   mod4
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod4.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod4
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod4;

	// Inputs
	reg [56:0] mantise;

	// Outputs
	wire semn;
	wire [47:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod4 uut (
		.mantise(mantise), 
		.semn(semn), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		mantise = 0;

		// Wait 100 ns for global reset to finish
		#100;
       mantise = 57'b100000001110011001100110011001100101010110011001100110011;
										
		// Add stimulus here

	end
      
endmodule

