`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:38:31 11/10/2016 
// Design Name: 
// Module Name:    part3 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module part3(clr,reset,clk,op_in,exps_in,mantise_in,mantise_out,exps_out,semn);

	input clr;
	input reset;
	input clk;
	input op_in;
	input wire[7:0] exps_in;
	output wire[7:0] exps_out;
	input wire[47:0] mantise_in;
	output wire[25:0] mantise_out;
	wire[25:0] mantise_temp;
	input wire semn;
	
	reg_8 reg_exps(
		.clk(clk),
		.reset(reset),
		.in(exps_in),
		.out(exps_out)	
	);	
	
	mod5 mod5(
		.mantise(mantise_in),
		.op(op_in),
		.semn(semn),
		.out(mantise_temp)
	);	
	
	reg_26 reg_mant(
		.clk(clk),
		.reset(reset),
		.in(mantise_temp),
		.out(mantise_out)	
	);
	
endmodule
	
	