`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:01:12 10/27/2016 
// Design Name: 
// Module Name:    mod1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod2(exps, chosen);

	input[15:0] exps;
	output reg[7:0] chosen;
	//dam la iesire exponentul mai mare, care se gaseste pe primii 8 biti din exponentii 
	//obtinuti in part1
	always @(exps) begin
		chosen[7:0] = exps[15:8];
	end
	
endmodule
