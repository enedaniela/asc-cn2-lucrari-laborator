`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:01:12 11/10/2016 
// Design Name: 
// Module Name:    part0 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module part0(clr,reset,clk,op,exps_in,mantise_in,mantise_out,exps_out);

	input clr;
	input reset;
	input clk;
	input op;
	input wire[15:0] exps_in;
	output wire[15:0] exps_out;
	input wire[47:0] mantise_in;
	output wire[47:0] mantise_out;
	
	reg_16 reg_exps(
		.clk(clk),
		.reset(reset),
		.in(exps_in),
		.out(exps_out)
	);
	
	reg_48 reg_mant(
		.clk(clk),
		.reset(reset),
		.in(mantise_in),
		.out(mantise_out)
	);
	
endmodule
	