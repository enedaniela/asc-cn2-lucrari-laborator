`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:38:50 11/19/2016
// Design Name:   mod3
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod3.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod3
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod3;

	// Inputs
	reg [7:0] exps;
	reg [8:0] chosen;

	// Outputs
	wire [7:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod3 uut (
		.exps(exps), 
		.chosen(chosen), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		exps = 0;
		chosen = 0;

		// Wait 100 ns for global reset to finish
		#100;
      
		chosen = 9'b100000001;
		exps = 8'b10000010;
		// Add stimulus here

	end
      
endmodule

