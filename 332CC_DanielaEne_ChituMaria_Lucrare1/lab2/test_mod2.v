`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:32:30 11/14/2016
// Design Name:   mod2
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test_mod2.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod2
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mod2;

	// Inputs
	reg [15:0] exps;

	// Outputs
	wire [7:0] chosen;

	// Instantiate the Unit Under Test (UUT)
	mod2 uut (
		.exps(exps), 
		.chosen(chosen)
	);

	initial begin
		// Initialize Inputs
		exps = 0;

		// Wait 100 ns for global reset to finish
		#100;
		exps = 16'b1000001010000001;
        
		// Add stimulus here

	end
      
endmodule

