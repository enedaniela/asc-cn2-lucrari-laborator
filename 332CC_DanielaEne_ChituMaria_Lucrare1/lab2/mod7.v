`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:21:45 11/03/2016 
// Design Name: 
// Module Name:    mod7 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod7(mantise,chosen,out);

	input wire[47:0] mantise;
	input wire[8:0] chosen;
	output reg[56:0] out;

	always @ (mantise | chosen) begin
//concatenez diferenta dintre exponenti cu mantisele concatenate	
		out = {chosen, mantise};		
	end

endmodule
