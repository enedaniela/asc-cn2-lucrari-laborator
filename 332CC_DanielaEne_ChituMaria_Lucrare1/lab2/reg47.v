`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:13:46 11/14/2016 
// Design Name: 
// Module Name:    reg47 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module reg_47(clk, reset, in, out);
	
	input clk;
	input reset;
	input[47:0] in;
	output reg[47:0] out;

	always @(posedge clk or posedge reset)  
    begin  
      if (reset)  
        out = 0;  
      else  
        out = in;  
    end  

endmodule
