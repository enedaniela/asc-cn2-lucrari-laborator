`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:32:07 10/27/2016
// Design Name:   mod1
// Module Name:   T:/323CB/lab2/test_mod1.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod1
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mod1;

	// Inputs
	reg [15:0] exps;

	// Outputs
	wire [8:0] chosen;
	wire [15:0] exps_out;

	// Instantiate the Unit Under Test (UUT)
	mod1 uut (
		.exps(exps), 
		.chosen(chosen), 
		.exps_out(exps_out)
	);

	initial begin
		// Initialize Inputs
		exps = 16'b1111111011111111;

		// Wait 100 ns for global reset to finish
		#100;
		exps = 16'b1111111111111110;
        
		// Add stimulus here

	end
      
endmodule

