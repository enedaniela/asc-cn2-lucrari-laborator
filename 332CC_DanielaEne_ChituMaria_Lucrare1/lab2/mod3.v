`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:01:12 10/27/2016 
// Design Name: 
// Module Name:    mod1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod3(exps, chosen, out);

	input[7:0] exps;
	input wire[8:0] chosen;
	output reg[7:0] out;
	
	always @(chosen | exps) begin
		if (chosen[8] == 1'b0) begin
			out = exps;			
		end
		else begin
			out = exps + chosen[7:0];
		end
	end
	
endmodule
