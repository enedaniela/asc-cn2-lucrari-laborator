`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:09:34 11/10/2016 
// Design Name: 
// Module Name:    part1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module part1(clr,reset,clk,op_in,exps_in,mantise_in,mantise_out,exps_out);

	input clr;
	input reset;
	input clk;
	input op_in;
	input wire[15:0] exps_in;
	output wire[15:0] exps_out;
	input wire[47:0] mantise_in; 
	output wire[56:0] mantise_out;
	wire[8:0] chosen;
	wire[15:0] exps_temp;
	wire[56:0] mantise_temp;
	
	mod1 mod1(
		.exps(exps_in),
		.chosen(chosen),
		.out(exps_temp)	
	);
	
	reg_16 reg_exps(
		.clk(clk),
		.reset(reset),
		.in(exps_temp),
		.out(exps_out)	
	);
	
	mod7 mod7(
		.mantise(mantise_in),
		.chosen(chosen),
		.out(mantise_temp)	
	);
	
	reg57 reg_mant(
		.clk(clk),
		.reset(reset),
		.in(mantise_temp),
		.out(mantise_out)	
	);
	
endmodule