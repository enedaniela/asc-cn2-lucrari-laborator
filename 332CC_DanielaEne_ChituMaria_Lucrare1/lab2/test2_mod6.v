`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:32:49 11/19/2016
// Design Name:   mod6
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod6.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod6
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod6;

	// Inputs
	reg [25:0] mantise;

	// Outputs
	wire [8:0] chosen;
	wire [23:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod6 uut (
		.mantise(mantise), 
		.chosen(chosen), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		mantise = 0;

		// Wait 100 ns for global reset to finish
		#100;
      mantise = 26'b11000100011001100110011001;
		// Add stimulus here

	end
      
endmodule

