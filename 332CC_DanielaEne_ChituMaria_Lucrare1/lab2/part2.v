`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:21:23 11/10/2016 
// Design Name: 
// Module Name:    part2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module part2(clr,reset,clk,op_in,exps_in,mantise_in,mantise_out,exps_out,semn);

	input clr;
	input reset;
	input clk;
	input op_in;
	input wire[15:0] exps_in;
	output wire[7:0] exps_out;
	input wire[56:0] mantise_in;
	output wire[47:0] mantise_out;
	wire[7:0] exps_temp;
	wire[47:0] mantise_temp;
	output wire semn;
	
	mod2 mod2(
		.exps(exps_in),
		.chosen(exps_temp)
	);
	
	reg_8 reg_exps(
		.clk(clk),
		.reset(reset),
		.in(exps_temp),
		.out(exps_out)	
	);	
	
	mod4 mod4(
		.mantise(mantise_in),
		.semn(semn),
		.out(mantise_temp)
	);	
	
	reg_47 reg_mant(
		.clk(clk),
		.reset(reset),
		.in(mantise_temp),
		.out(mantise_out)	
	);
	
endmodule
	
	