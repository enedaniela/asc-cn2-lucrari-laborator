/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/dana/Desktop/To clean/lab4/lab2/mod7.v";



static void Always_27_0(char *t0)
{
    char t4[16];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;

LAB0:    t1 = (t0 + 2688U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(27, ng0);
    t2 = (t0 + 3256);
    *((int *)t2) = 1;
    t3 = (t0 + 2720);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(27, ng0);

LAB5:    xsi_set_current_line(29, ng0);
    t5 = (t0 + 1048U);
    t6 = *((char **)t5);
    t5 = (t0 + 1208U);
    t7 = *((char **)t5);
    xsi_vlogtype_concat(t4, 57, 57, 2U, t7, 9, t6, 48);
    t5 = (t0 + 1608);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 57);
    goto LAB2;

}

static void impl1_execute(char *t0)
{
    char t7[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    unsigned int t19;
    char *t20;
    unsigned int t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    unsigned int t32;
    char *t33;
    char *t34;
    unsigned int t35;
    char *t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    char *t51;
    unsigned int t52;
    char *t53;
    unsigned int t54;
    char *t55;
    unsigned int t56;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    char *t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;

LAB0:    t1 = (t0 + 2936U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    t2 = (t0 + 3272);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    t3 = (t0 + 1768);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    t4 = (t0 + 1208U);
    t6 = *((char **)t4);
    t8 = 0;

LAB8:    t9 = (t8 < 1);
    if (t9 == 1)
        goto LAB9;

LAB10:    t8 = 1;

LAB11:    t49 = (t8 < 2);
    if (t49 == 1)
        goto LAB12;

LAB17:    xsi_vlogimplicitvar_assign_value(t3, t7, 48);
    goto LAB2;

LAB5:    t28 = (t8 * 8);
    t29 = *((unsigned int *)t12);
    t30 = *((unsigned int *)t22);
    *((unsigned int *)t12) = (t29 | t30);
    t31 = (t5 + t28);
    t32 = (t28 + 4);
    t33 = (t5 + t32);
    t34 = (t6 + t28);
    t35 = (t28 + 4);
    t36 = (t6 + t35);
    t37 = *((unsigned int *)t33);
    t38 = (~(t37));
    t39 = *((unsigned int *)t31);
    t40 = (t39 & t38);
    t41 = *((unsigned int *)t36);
    t42 = (~(t41));
    t43 = *((unsigned int *)t34);
    t44 = (t43 & t42);
    t45 = (~(t40));
    t46 = (~(t44));
    t47 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t47 & t45);
    t48 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t48 & t46);

LAB7:    t8 = (t8 + 1);
    goto LAB8;

LAB6:    goto LAB7;

LAB9:    t10 = (t8 * 8);
    t4 = (t5 + t10);
    t11 = (t6 + t10);
    t12 = (t7 + t10);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t11);
    t15 = (t13 | t14);
    *((unsigned int *)t12) = t15;
    t16 = (t8 * 8);
    t17 = (t16 + 4);
    t18 = (t5 + t17);
    t19 = (t16 + 4);
    t20 = (t6 + t19);
    t21 = (t16 + 4);
    t22 = (t7 + t21);
    t23 = *((unsigned int *)t18);
    t24 = *((unsigned int *)t20);
    t25 = (t23 | t24);
    *((unsigned int *)t22) = t25;
    t26 = *((unsigned int *)t22);
    t27 = (t26 != 0);
    if (t27 == 1)
        goto LAB5;
    else
        goto LAB6;

LAB12:    t50 = (t8 * 8);
    t51 = (t7 + t50);
    *((unsigned int *)t51) = 0;
    t52 = (t50 + 4);
    t53 = (t7 + t52);
    *((unsigned int *)t53) = 0;
    t54 = (t8 < 2);
    if (t54 == 1)
        goto LAB13;

LAB15:    t61 = (t8 < 1);
    if (t61 == 1)
        goto LAB14;

LAB16:    t8 = (t8 + 1);
    goto LAB11;

LAB13:    t55 = (t5 + t50);
    t56 = (t50 + 4);
    t57 = (t5 + t56);
    t58 = *((unsigned int *)t55);
    t59 = *((unsigned int *)t57);
    t60 = (t58 | t59);
    *((unsigned int *)t51) = t60;
    *((unsigned int *)t53) = *((unsigned int *)t57);
    goto LAB16;

LAB14:    t62 = (t6 + t50);
    t63 = (t50 + 4);
    t64 = (t6 + t63);
    t65 = *((unsigned int *)t62);
    t66 = *((unsigned int *)t64);
    t67 = (t65 | t66);
    *((unsigned int *)t51) = t67;
    *((unsigned int *)t53) = *((unsigned int *)t64);
    goto LAB16;

}


extern void work_m_00000000003568699623_3163111224_init()
{
	static char *pe[] = {(void *)Always_27_0,(void *)impl1_execute};
	xsi_register_didat("work_m_00000000003568699623_3163111224", "isim/testAll_isim_beh.exe.sim/work/m_00000000003568699623_3163111224.didat");
	xsi_register_executes(pe);
}
