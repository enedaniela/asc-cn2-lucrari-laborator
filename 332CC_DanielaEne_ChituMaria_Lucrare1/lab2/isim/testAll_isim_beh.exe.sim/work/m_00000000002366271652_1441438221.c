/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/dana/Desktop/To clean/lab4/lab2/mod1.v";
static unsigned int ng1[] = {0U, 0U};
static int ng2[] = {8, 0};
static unsigned int ng3[] = {1U, 0U};



static void Always_27_0(char *t0)
{
    char t4[8];
    char t14[8];
    char t24[8];
    char t49[8];
    char t50[8];
    char t51[8];
    char t54[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    char *t39;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    char *t46;
    char *t47;
    int t48;
    char *t52;
    char *t53;
    char *t55;
    char *t56;

LAB0:    t1 = (t0 + 2528U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(27, ng0);
    t2 = (t0 + 2848);
    *((int *)t2) = 1;
    t3 = (t0 + 2560);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(27, ng0);

LAB5:    xsi_set_current_line(29, ng0);
    t5 = (t0 + 1048U);
    t6 = *((char **)t5);
    memset(t4, 0, 8);
    t5 = (t4 + 4);
    t7 = (t6 + 4);
    t8 = *((unsigned int *)t6);
    t9 = (t8 >> 8);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t7);
    t11 = (t10 >> 8);
    *((unsigned int *)t5) = t11;
    t12 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t12 & 255U);
    t13 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t13 & 255U);
    t15 = (t0 + 1048U);
    t16 = *((char **)t15);
    memset(t14, 0, 8);
    t15 = (t14 + 4);
    t17 = (t16 + 4);
    t18 = *((unsigned int *)t16);
    t19 = (t18 >> 0);
    *((unsigned int *)t14) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 0);
    *((unsigned int *)t15) = t21;
    t22 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t22 & 255U);
    t23 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t23 & 255U);
    memset(t24, 0, 8);
    t25 = (t4 + 4);
    t26 = (t14 + 4);
    t27 = *((unsigned int *)t4);
    t28 = *((unsigned int *)t14);
    t29 = (t27 ^ t28);
    t30 = *((unsigned int *)t25);
    t31 = *((unsigned int *)t26);
    t32 = (t30 ^ t31);
    t33 = (t29 | t32);
    t34 = *((unsigned int *)t25);
    t35 = *((unsigned int *)t26);
    t36 = (t34 | t35);
    t37 = (~(t36));
    t38 = (t33 & t37);
    if (t38 != 0)
        goto LAB9;

LAB6:    if (t36 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t24) = 1;

LAB9:    t40 = (t24 + 4);
    t41 = *((unsigned int *)t40);
    t42 = (~(t41));
    t43 = *((unsigned int *)t24);
    t44 = (t43 & t42);
    t45 = (t44 != 0);
    if (t45 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(33, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t4 + 4);
    t5 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 8);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t5);
    t11 = (t10 >> 8);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t6 = (t0 + 1048U);
    t7 = *((char **)t6);
    memset(t14, 0, 8);
    t6 = (t14 + 4);
    t15 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t14) = t19;
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t6) = t21;
    t22 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t22 & 255U);
    t23 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t23 & 255U);
    memset(t24, 0, 8);
    t16 = (t4 + 4);
    if (*((unsigned int *)t16) != 0)
        goto LAB17;

LAB16:    t17 = (t14 + 4);
    if (*((unsigned int *)t17) != 0)
        goto LAB17;

LAB20:    if (*((unsigned int *)t4) > *((unsigned int *)t14))
        goto LAB18;

LAB19:    t26 = (t24 + 4);
    t27 = *((unsigned int *)t26);
    t28 = (~(t27));
    t29 = *((unsigned int *)t24);
    t30 = (t29 & t28);
    t31 = (t30 != 0);
    if (t31 > 0)
        goto LAB21;

LAB22:    xsi_set_current_line(37, ng0);

LAB25:    xsi_set_current_line(38, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t14, 0, 8);
    t2 = (t14 + 4);
    t5 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 0);
    *((unsigned int *)t14) = t9;
    t10 = *((unsigned int *)t5);
    t11 = (t10 >> 0);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t6 = (t0 + 1048U);
    t7 = *((char **)t6);
    memset(t24, 0, 8);
    t6 = (t24 + 4);
    t15 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 8);
    *((unsigned int *)t24) = t19;
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 8);
    *((unsigned int *)t6) = t21;
    t22 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t22 & 255U);
    t23 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t23 & 255U);
    memset(t49, 0, 8);
    xsi_vlog_unsigned_minus(t49, 8, t14, 8, t24, 8);
    t16 = ((char*)((ng1)));
    xsi_vlogtype_concat(t4, 9, 9, 2U, t16, 1, t49, 8);
    t17 = (t0 + 1448);
    xsi_vlogvar_assign_value(t17, t4, 0, 0, 9);
    xsi_set_current_line(39, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t14, 0, 8);
    t2 = (t14 + 4);
    t5 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 8);
    *((unsigned int *)t14) = t9;
    t10 = *((unsigned int *)t5);
    t11 = (t10 >> 8);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t6 = (t0 + 1048U);
    t7 = *((char **)t6);
    memset(t24, 0, 8);
    t6 = (t24 + 4);
    t15 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t24) = t19;
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t6) = t21;
    t22 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t22 & 255U);
    t23 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t23 & 255U);
    xsi_vlogtype_concat(t4, 16, 16, 2U, t24, 8, t14, 8);
    t16 = (t0 + 1608);
    xsi_vlogvar_assign_value(t16, t4, 0, 0, 16);

LAB23:
LAB12:    goto LAB2;

LAB8:    t39 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(29, ng0);

LAB13:    xsi_set_current_line(30, ng0);
    t46 = ((char*)((ng1)));
    t47 = (t0 + 1448);
    xsi_vlogvar_assign_value(t47, t46, 0, 0, 9);
    xsi_set_current_line(31, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 1448);
    t5 = (t0 + 1448);
    t6 = (t5 + 72U);
    t7 = *((char **)t6);
    t15 = ((char*)((ng2)));
    xsi_vlog_generic_convert_bit_index(t4, t7, 2, t15, 32, 1);
    t16 = (t4 + 4);
    t8 = *((unsigned int *)t16);
    t48 = (!(t8));
    if (t48 == 1)
        goto LAB14;

LAB15:    goto LAB12;

LAB14:    xsi_vlogvar_assign_value(t3, t2, 0, *((unsigned int *)t4), 1);
    goto LAB15;

LAB17:    t25 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB19;

LAB18:    *((unsigned int *)t24) = 1;
    goto LAB19;

LAB21:    xsi_set_current_line(33, ng0);

LAB24:    xsi_set_current_line(34, ng0);
    t39 = (t0 + 1048U);
    t40 = *((char **)t39);
    memset(t50, 0, 8);
    t39 = (t50 + 4);
    t46 = (t40 + 4);
    t32 = *((unsigned int *)t40);
    t33 = (t32 >> 8);
    *((unsigned int *)t50) = t33;
    t34 = *((unsigned int *)t46);
    t35 = (t34 >> 8);
    *((unsigned int *)t39) = t35;
    t36 = *((unsigned int *)t50);
    *((unsigned int *)t50) = (t36 & 255U);
    t37 = *((unsigned int *)t39);
    *((unsigned int *)t39) = (t37 & 255U);
    t47 = (t0 + 1048U);
    t52 = *((char **)t47);
    memset(t51, 0, 8);
    t47 = (t51 + 4);
    t53 = (t52 + 4);
    t38 = *((unsigned int *)t52);
    t41 = (t38 >> 0);
    *((unsigned int *)t51) = t41;
    t42 = *((unsigned int *)t53);
    t43 = (t42 >> 0);
    *((unsigned int *)t47) = t43;
    t44 = *((unsigned int *)t51);
    *((unsigned int *)t51) = (t44 & 255U);
    t45 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t45 & 255U);
    memset(t54, 0, 8);
    xsi_vlog_unsigned_minus(t54, 8, t50, 8, t51, 8);
    t55 = ((char*)((ng3)));
    xsi_vlogtype_concat(t49, 9, 9, 2U, t55, 1, t54, 8);
    t56 = (t0 + 1448);
    xsi_vlogvar_assign_value(t56, t49, 0, 0, 9);
    xsi_set_current_line(35, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t14, 0, 8);
    t2 = (t14 + 4);
    t5 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 0);
    *((unsigned int *)t14) = t9;
    t10 = *((unsigned int *)t5);
    t11 = (t10 >> 0);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t12 & 255U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 255U);
    t6 = (t0 + 1048U);
    t7 = *((char **)t6);
    memset(t24, 0, 8);
    t6 = (t24 + 4);
    t15 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 8);
    *((unsigned int *)t24) = t19;
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 8);
    *((unsigned int *)t6) = t21;
    t22 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t22 & 255U);
    t23 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t23 & 255U);
    xsi_vlogtype_concat(t4, 16, 16, 2U, t24, 8, t14, 8);
    t16 = (t0 + 1608);
    xsi_vlogvar_assign_value(t16, t4, 0, 0, 16);
    goto LAB23;

}


extern void work_m_00000000002366271652_1441438221_init()
{
	static char *pe[] = {(void *)Always_27_0};
	xsi_register_didat("work_m_00000000002366271652_1441438221", "isim/testAll_isim_beh.exe.sim/work/m_00000000002366271652_1441438221.didat");
	xsi_register_executes(pe);
}
