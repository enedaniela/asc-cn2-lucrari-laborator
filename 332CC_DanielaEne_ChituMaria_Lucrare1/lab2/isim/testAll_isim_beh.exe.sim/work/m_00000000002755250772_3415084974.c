/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/dana/Desktop/To clean/lab4/lab2/mod6.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1U, 0U};
static int ng3[] = {8, 0};
static int ng4[] = {7, 0};
static int ng5[] = {0, 0};
static int ng6[] = {22, 0};
static int ng7[] = {23, 0};



static void Always_30_0(char *t0)
{
    char t6[8];
    char t15[8];
    char t37[8];
    char t45[8];
    char t46[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;
    char *t36;
    char *t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    unsigned int t43;
    int t44;
    int t47;
    int t48;
    int t49;
    int t50;
    int t51;
    int t52;
    int t53;

LAB0:    t1 = (t0 + 2848U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(30, ng0);
    t2 = (t0 + 3168);
    *((int *)t2) = 1;
    t3 = (t0 + 2880);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(30, ng0);

LAB5:    xsi_set_current_line(31, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t6, 0, 8);
    t4 = (t6 + 4);
    t7 = (t5 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 25);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t7);
    t12 = (t11 >> 25);
    t13 = (t12 & 1);
    *((unsigned int *)t4) = t13;
    t14 = (t0 + 1768);
    xsi_vlogvar_assign_value(t14, t6, 0, 0, 1);
    xsi_set_current_line(32, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 1);
    *((unsigned int *)t6) = t9;
    t10 = *((unsigned int *)t4);
    t11 = (t10 >> 1);
    *((unsigned int *)t2) = t11;
    t12 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t12 & 16777215U);
    t13 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t13 & 16777215U);
    t5 = (t0 + 1928);
    xsi_vlogvar_assign_value(t5, t6, 0, 0, 24);
    xsi_set_current_line(33, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 1448);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 9);
    xsi_set_current_line(34, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 24);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 24);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = ((char*)((ng2)));
    memset(t15, 0, 8);
    t7 = (t6 + 4);
    t14 = (t5 + 4);
    t16 = *((unsigned int *)t6);
    t17 = *((unsigned int *)t5);
    t18 = (t16 ^ t17);
    t19 = *((unsigned int *)t7);
    t20 = *((unsigned int *)t14);
    t21 = (t19 ^ t20);
    t22 = (t18 | t21);
    t23 = *((unsigned int *)t7);
    t24 = *((unsigned int *)t14);
    t25 = (t23 | t24);
    t26 = (~(t25));
    t27 = (t22 & t26);
    if (t27 != 0)
        goto LAB9;

LAB6:    if (t25 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t15) = 1;

LAB9:    t29 = (t15 + 4);
    t30 = *((unsigned int *)t29);
    t31 = (~(t30));
    t32 = *((unsigned int *)t15);
    t33 = (t32 & t31);
    t34 = (t33 != 0);
    if (t34 > 0)
        goto LAB10;

LAB11:
LAB12:    xsi_set_current_line(38, ng0);
    t2 = (t0 + 1928);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    memset(t6, 0, 8);
    t5 = (t6 + 4);
    t7 = (t4 + 4);
    t8 = *((unsigned int *)t4);
    t9 = (t8 >> 0);
    *((unsigned int *)t6) = t9;
    t10 = *((unsigned int *)t7);
    t11 = (t10 >> 0);
    *((unsigned int *)t5) = t11;
    t12 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t12 & 8388607U);
    t13 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t13 & 8388607U);
    t14 = (t0 + 1608);
    t28 = (t0 + 1608);
    t29 = (t28 + 72U);
    t35 = *((char **)t29);
    t36 = ((char*)((ng6)));
    t38 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t15, t37, t45, ((int*)(t35)), 2, t36, 32, 1, t38, 32, 1);
    t39 = (t15 + 4);
    t16 = *((unsigned int *)t39);
    t44 = (!(t16));
    t40 = (t37 + 4);
    t17 = *((unsigned int *)t40);
    t47 = (!(t17));
    t48 = (t44 && t47);
    t41 = (t45 + 4);
    t18 = *((unsigned int *)t41);
    t49 = (!(t18));
    t50 = (t48 && t49);
    if (t50 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(39, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1608);
    t7 = (t0 + 1608);
    t14 = (t7 + 72U);
    t28 = *((char **)t14);
    t29 = ((char*)((ng7)));
    xsi_vlog_generic_convert_bit_index(t6, t28, 2, t29, 32, 1);
    t35 = (t6 + 4);
    t8 = *((unsigned int *)t35);
    t44 = (!(t8));
    if (t44 == 1)
        goto LAB20;

LAB21:    goto LAB2;

LAB8:    t28 = (t15 + 4);
    *((unsigned int *)t15) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(34, ng0);

LAB13:    xsi_set_current_line(35, ng0);
    t35 = ((char*)((ng2)));
    t36 = (t0 + 1448);
    t38 = (t0 + 1448);
    t39 = (t38 + 72U);
    t40 = *((char **)t39);
    t41 = ((char*)((ng3)));
    xsi_vlog_generic_convert_bit_index(t37, t40, 2, t41, 32, 1);
    t42 = (t37 + 4);
    t43 = *((unsigned int *)t42);
    t44 = (!(t43));
    if (t44 == 1)
        goto LAB14;

LAB15:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 1448);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    memset(t6, 0, 8);
    t5 = (t6 + 4);
    t7 = (t4 + 4);
    t8 = *((unsigned int *)t4);
    t9 = (t8 >> 0);
    *((unsigned int *)t6) = t9;
    t10 = *((unsigned int *)t7);
    t11 = (t10 >> 0);
    *((unsigned int *)t5) = t11;
    t12 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t12 & 255U);
    t13 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t13 & 255U);
    t14 = ((char*)((ng2)));
    memset(t15, 0, 8);
    xsi_vlog_unsigned_add(t15, 8, t6, 8, t14, 8);
    t28 = (t0 + 1448);
    t29 = (t0 + 1448);
    t35 = (t29 + 72U);
    t36 = *((char **)t35);
    t38 = ((char*)((ng4)));
    t39 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t37, t45, t46, ((int*)(t36)), 2, t38, 32, 1, t39, 32, 1);
    t40 = (t37 + 4);
    t16 = *((unsigned int *)t40);
    t44 = (!(t16));
    t41 = (t45 + 4);
    t17 = *((unsigned int *)t41);
    t47 = (!(t17));
    t48 = (t44 && t47);
    t42 = (t46 + 4);
    t18 = *((unsigned int *)t42);
    t49 = (!(t18));
    t50 = (t48 && t49);
    if (t50 == 1)
        goto LAB16;

LAB17:    goto LAB12;

LAB14:    xsi_vlogvar_assign_value(t36, t35, 0, *((unsigned int *)t37), 1);
    goto LAB15;

LAB16:    t19 = *((unsigned int *)t46);
    t51 = (t19 + 0);
    t20 = *((unsigned int *)t37);
    t21 = *((unsigned int *)t45);
    t52 = (t20 - t21);
    t53 = (t52 + 1);
    xsi_vlogvar_assign_value(t28, t15, t51, *((unsigned int *)t45), t53);
    goto LAB17;

LAB18:    t19 = *((unsigned int *)t45);
    t51 = (t19 + 0);
    t20 = *((unsigned int *)t15);
    t21 = *((unsigned int *)t37);
    t52 = (t20 - t21);
    t53 = (t52 + 1);
    xsi_vlogvar_assign_value(t14, t6, t51, *((unsigned int *)t37), t53);
    goto LAB19;

LAB20:    xsi_vlogvar_assign_value(t5, t4, 0, *((unsigned int *)t6), 1);
    goto LAB21;

}


extern void work_m_00000000002755250772_3415084974_init()
{
	static char *pe[] = {(void *)Always_30_0};
	xsi_register_didat("work_m_00000000002755250772_3415084974", "isim/testAll_isim_beh.exe.sim/work/m_00000000002755250772_3415084974.didat");
	xsi_register_executes(pe);
}
