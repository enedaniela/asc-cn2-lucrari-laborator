`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:42:13 11/10/2016 
// Design Name: 
// Module Name:    pipe 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pipe(clr, reset, clk, op1, op2, op, rezmant, rezexp);
	
	input clr;
	input reset;
	input clk;
	input op;
	input wire[31:0] op1, op2;
	
	output wire[23:0] rezmant;
	output wire[7:0] rezexp;


	wire[15:0] exps_part0;
	wire[47:0] mantise_part0;
	wire[15:0] exps_part1;
	wire[56:0] mantise_part1;
	wire[7:0] exps_part2;
	wire[47:0] mantise_part2;
	wire semn;
	wire[7:0] exps_part3;
	wire[25:0] mantise_part3;	
	wire[7:0] exps_part4;
	wire[23:0] mantise_part4;

	part0 part0(
		.clr(clr),
		.reset(reset),
		.clk(clk),
		.op(op),
		.exps_in({op1[30:23], op2[30:23]}),
		.mantise_in({op1[23:0], op2[23:0]}),
		.exps_out(exps_part0),
		.mantise_out(mantise_part0)
	);
	
	part1 part1(
		.clr(clr),
		.reset(reset),
		.clk(clk),
		.op_in(op),
		.exps_in(exps_part0),
		.mantise_in(mantise_part0),
		.exps_out(exps_part1),
		.mantise_out(mantise_part1)
	);
	
	part2 part2(
		.clr(clr),
		.reset(reset),
		.clk(clk),
		.op_in(op),
		.exps_in(exps_part1),
		.mantise_in(mantise_part1),
		.exps_out(exps_part2),
		.mantise_out(mantise_part2),
		.semn(semn)
	);
	
	part3 part3(
		.clr(clr),
		.reset(reset),
		.clk(clk),
		.op_in(op),
		.exps_in(exps_part2),
		.mantise_in(mantise_part2),
		.exps_out(exps_part3),
		.mantise_out(mantise_part3),
		.semn(semn)
	);
	
	part4 part4(
		.clr(clr),
		.reset(reset),
		.clk(clk),
		.op_in(op),
		.exps_in(exps_part3),
		.mantise_in(mantise_part3),
		.exps_out(exps_part4),
		.mantise_out(mantise_part4)
	);

	assign rezmant = mantise_part4;
	assign rezexp = {mantise_part4[23],exps_part4[7:1]};
endmodule
