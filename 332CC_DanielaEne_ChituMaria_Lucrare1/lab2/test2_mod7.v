`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:28:57 11/14/2016
// Design Name:   mod7
// Module Name:   C:/Users/dana/Desktop/lab4/lab2/test2_mod7.v
// Project Name:  lab2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mod7
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test2_mod7;

	// Inputs
	reg [47:0] mantise;
	reg [8:0] chosen;

	// Outputs
	wire [56:0] out;

	// Instantiate the Unit Under Test (UUT)
	mod7 uut (
		.mantise(mantise), 
		.chosen(chosen), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		mantise = 0;
		chosen = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		chosen = 9'b100000001;
		mantise = 48'b101010110011001100110011110011001100110011001100;
						  
	end
      
endmodule

