`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:20:36 11/14/2016 
// Design Name: 
// Module Name:    reg57 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module reg57(clk, reset, in, out);
	
	input clk;
	input reset;
	input[56:0] in;
	output reg[56:0] out;

	always @(posedge clk or posedge reset)  
    begin  
      if (reset)  
        out = 0;  
      else  
        out = in;  
    end  

endmodule
