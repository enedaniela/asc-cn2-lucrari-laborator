`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:11:11 11/03/2016 
// Design Name: 
// Module Name:    mod5 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod5(mantise,op,out,semn);

	input op;
	input wire[47:0] mantise; 
	input wire semn;
	output reg[25:0] out;
	
	always @ (mantise | op | semn) begin
		if (op) begin
			out[24:0] = mantise[47:24] - mantise[23:0];
		end
		else begin
			out[24:0] = mantise[47:24] + mantise[23:0];
		end
		
		out[25] = semn ^(out[24] & op);
	end


endmodule
