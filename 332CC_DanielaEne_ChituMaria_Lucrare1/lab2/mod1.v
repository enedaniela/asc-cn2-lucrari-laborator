`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:01:12 10/27/2016 
// Design Name: 
// Module Name:    mod1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mod1(exps, chosen, out);

	input wire[15:0] exps;
	output reg[8:0] chosen;
	output reg[15:0] out;
//returneaza exponentul mai mare si exponentii cu cel mai mare primul
	always @(exps) begin	
	
		if(exps[15:8] == exps[7:0]) begin
			chosen = 8'b0; // diferenta dintre exponenti
			chosen[8] = 1'b0;
		end
		else if (exps[15:8] > exps[7:0]) begin
			chosen = {1'b1, (exps[15:8] - exps[7:0])};
			out = {exps[15:8], exps[7:0]};
		end
		else begin
			chosen = {1'b0,(exps[7:0] - exps[15:8])};
			out = {exps[7:0], exps[15:8]};
		end
		
		
	end
	
endmodule
