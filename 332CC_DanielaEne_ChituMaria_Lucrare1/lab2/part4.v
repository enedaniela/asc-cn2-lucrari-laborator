`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:31:11 11/10/2016 
// Design Name: 
// Module Name:    part4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module part4(clr,reset,clk,op_in,exps_in,mantise_in,mantise_out,exps_out);

	input clr;
	input reset;
	input clk;
	input op_in;
	input wire[7:0] exps_in;
	output wire[7:0] exps_out;
	input wire[25:0] mantise_in;
	output wire[23:0] mantise_out;
	wire[8:0] chosen;
	wire[23:0] mantise_temp;
	wire[7:0] exps_temp;
	
	mod6 mod6(
		.mantise(mantise_in),
		.chosen(chosen),
		.out(mantise_temp)
	);	
	
	reg_24 reg_mant(
		.clk(clk),
		.reset(reset),
		.in(mantise_temp),
		.out(mantise_out)	
	);	
	
	mod3 mod3(
		.exps(exps_in),
		.chosen(chosen),
		.out(exps_temp)	
	);
	
	reg_8 reg_exps(
		.clk(clk),
		.reset(reset),
		.in(exps_temp),
		.out(exps_out)	
	);	
	
endmodule
	
	