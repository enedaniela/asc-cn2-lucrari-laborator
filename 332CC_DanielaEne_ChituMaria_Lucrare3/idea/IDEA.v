`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:33:37 01/12/2017 
// Design Name: 
// Module Name:    IDEA 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IDEA(inp, ikey, clk, result);

	input [0:63] inp;
	input [0:127] ikey;
	input clk;
	output reg [0:63] result;
	
	wire [0:15] steps[0:13];
	reg [0:15] step[0:13];
	reg [0:15] data[0:3];
	wire [0:15] datas[0:3];
	reg [0:127] key;
	
	initial begin
		data[0][0:15] = inp[0:15];
		data[1][0:15] = inp[16:31];
		data[2][0:15] = inp[32:47];
		data[3][0:15] = inp[48:63];
		key = ikey;
		step[4] = 0;
		step[7] = 0;
	end
	
	reg [0:4] i;
	reg [0:15] tmp_d0;
	reg [0:15] tmp_d3;
	reg [0:127] tmp_key;

	mul mul1(.a(data[0][0:15]), .b(key[0:15]), .result(steps[0]));
	mul mul2(.a(data[3][0:15]), .b(key[48:63]), .result(steps[3]));
	mul mul3(.a(step[4]), .b(key[64:79]), .result(steps[6]));
	mul mul4(.a(step[7]), .b(key[80:95]), .result(steps[8]));
	
	mul mul_d1(.a(tmp_d0), .b(key[0:15]), .result(datas[0]));
	mul mul_d3(.a(tmp_d3), .b(key[48:63]), .result(datas[3]));
	
	always @(posedge clk) begin
		
		for(i = 0 ; i < 8 ; i = i + 1) begin
		
	mul mul1(.a(data[0][0:15]), .b(key[0:15]), .result(steps[0]));
	mul mul2(.a(data[3][0:15]), .b(key[48:63]), .result(steps[3]));
	mul mul3(.a(step[4]), .b(key[64:79]), .result(steps[6]));
	mul mul4(.a(step[7]), .b(key[80:95]), .result(steps[8]));
	
	mul mul_d1(.a(tmp_d0), .b(key[0:15]), .result(datas[0]));
	mul mul_d3(.a(tmp_d3), .b(key[48:63]), .result(datas[3]));
			
			step[1] = data[1] + key[16:31];
			step[2] = data[2] + key[32:47];
			step[3] = steps[3];
			step[4] = step[0] ^ step[2];
			step[5] = step[1] ^ step[3];
			step[6] = steps[6];
			step[7] = step[5] + step[6];
			step[8] = steps[8];
			step[9] = step[6] + step[8];
			step[10] =step[0] ^ step[8];
			step[11] =step[2] ^ step[8];
			step[12] =step[1] ^ step[9];
			step[13] =step[3] ^ step[9];
			
			data[0] = step[10];
			data[1] = step[11];
			data[2] = step[12];
			data[3] = step[13];
			
			tmp_key = key;
			key = key << 25;
			key[0:24] = tmp_key[103:127];
			
		end	
		
		data[1] = step[12];
		data[2] = step[11];
		
		tmp_d0 = data[0];
		data[0] = datas[0];
		data[1] = data[1] + key[16:31];
		data[2] = data[2] + key[32:47];
		tmp_d3 = data[3];
		data[3] = datas[3];
			
		result = {data[0], data[1], data[2], data[3]};
		
	end
	
endmodule
