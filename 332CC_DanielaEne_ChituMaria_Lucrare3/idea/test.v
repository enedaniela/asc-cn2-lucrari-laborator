`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:36:00 01/12/2017
// Design Name:   IDEA
// Module Name:   C:/Users/denisa/Downloads/lucrare2/idea/test.v
// Project Name:  idea
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: IDEA
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test;

	// Inputs
	reg [0:63] inp;
	reg [0:127] key;
	reg clk;

	// Outputs
	wire [0:63] result;

	// Instantiate the Unit Under Test (UUT)
	IDEA uut (
		.inp(inp), 
		.ikey(key), 
		.clk(clk), 
		.result(result)
	);

	initial begin
		// Initialize Inputs
		inp = 0;
		key = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		inp = 64'hf5db1ac45e5ef9f9;
		key = 128'h9d4075c103bc322afb03e7be6ab30006;
	end
      
	
	always begin
		#5 clk = ~clk;
	end
endmodule

