`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:18 01/12/2017 
// Design Name: 
// Module Name:    mul 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mul(a,b,result);
	
	input [0:15] a;
	input [0:15] b;
	output reg [0:15] result;
	reg [0:31] p;
	reg [0:31] A, B;
	
	always @* begin
		A = 0; B = 0;
		A[0:15] = a; B[0:15] = b;
		
		if(A != 0) begin
			if(B != 0) begin
				p = A * B;
				B = p & 16'hFFFF;
				A = p >> 16;
				result = (B - A + (B < A ? 1 : 0));
			end
			else begin 
				result = 1 - A;
			end
		end else begin
			result = 1 - B;
		end
		
	end

endmodule
