`timescale 1ns / 1ps

module read_crc_table;

integer k;
reg [15:0] crctable[0:255];
integer data_file, scan_file;

initial begin
	$dumpfile("simple.vcd");
	$dumpvars(0, read_crc_table);
	#1500;
	$finish;
end

initial begin
  data_file = $fopen("crc_table.txt", "r");
  if (data_file == 0) begin
    $display("data_file handle was NULL");
    $finish;
  end
  k = 0;
  while(! $feof(data_file)) begin
	scan_file = $fscanf(data_file, "%h %h %h %h %h %h %h %h\n", crctable[k], crctable[k+1], crctable[k+2], crctable[k+3], crctable[k+4], crctable[k+5], crctable[k+6], crctable[k+7]);
	$display("%h %h %h %h %h %h %h %h\n", crctable[k], crctable[k+1], crctable[k+2], crctable[k+3], crctable[k+4], crctable[k+5], crctable[k+6], crctable[k+7]);
	k = k + 8;
  end
end


endmodule
