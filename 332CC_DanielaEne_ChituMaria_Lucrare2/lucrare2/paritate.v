`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:47:04 11/17/2016 
// Design Name: 
// Module Name:    circuit_paritate 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module circuit_paritate(date, paritate);

	input [7:0] date;
	output reg paritate;
	
	always @(date) begin
		paritate = date[0] ^ date[1] ^ date[2] ^ date[3] ^ date[4] ^ date[5] ^ date[6] ^ date[7];
	end	


endmodule
