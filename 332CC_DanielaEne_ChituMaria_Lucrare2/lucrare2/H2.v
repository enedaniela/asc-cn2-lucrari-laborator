`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:04:18 11/24/2016 
// Design Name: 
// Module Name:    H2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module H2(start, rxrdy, reset, num12, dreq, ceas, txrdy, rdy, incarca, depl, ddisp);

	input start, rxrdy, reset, num12, dreq, ceas;
	output reg txrdy, rdy, incarca, depl, ddisp;
	
	reg [2:0] S;
	reg [2:0] next_S;
	localparam S0 = 3'd0, S1 = 3'd1, S2 = 3'd2, S3 = 3'd3, S4 = 3'd4, S5 = 3'd5;
	
	initial begin
		S = S0;
		next_S = S0;
		txrdy = 0;
		depl = 0;
		ddisp = 0;
		incarca = 0;
		rdy = 0;
	end
	
	always @ (posedge ceas or reset) begin
	
		if (reset == 1) begin			
			S = S0;
			txrdy = 0;
			depl = 0;
			ddisp = 0;
			incarca = 0;
			rdy = 0;
		end
		else begin		
			S = next_S;				
		end
	end
	
	always @ (posedge ceas) begin
	
		txrdy = 0;
		depl = 0;
		ddisp = 0;
		incarca = 0;
		rdy = 0;
		next_S = S;
		
		case (S)
			S0: begin
					rdy = 1;
					if (start == 0) begin
						next_S = S1;
					end
			end
				
			S1: begin
				incarca = 1;
				next_S = S2;
			end
			
			S2: begin
				ddisp = 1;
				depl = 1;
				if (dreq == 0) begin
					next_S = S3;
				end
			end
			
			S3: begin
				depl = 1;
				next_S = S4;
			end
			
			S4: begin
				depl = 1;
				if (num12 == 1) begin
					next_S = S5;
				end
			end
			
			S5: begin
				txrdy = 1;
				if (rxrdy == 0) begin
					next_S = S0;
				end
				else begin
					next_S = S5;
				end
			end
			
			default: begin
				next_S = S0;
			end
		endcase
	end

endmodule
