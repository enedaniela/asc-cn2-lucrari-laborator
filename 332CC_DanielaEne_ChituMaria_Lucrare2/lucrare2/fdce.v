`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:32:16 11/24/2016 
// Design Name: 
// Module Name:    fdce 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fdce(CLR, CE, D, C, Q);

	input CLR, CE, D, C;
	output Q;
	
	
	always @(*) begin
		
		if (CLR == 1) begin
				Q = 0;
		end
		else begin
			if (CE == 1) begin
				if (D == 1) begin
					Q = 1;
				end
				else begin
					Q = 0;
				end
			end
		end
		
	end
	


endmodule
