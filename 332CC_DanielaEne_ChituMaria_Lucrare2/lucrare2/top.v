`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:40:35 11/24/2016 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(D, CLK, RESET, RXRDY, DREQ, START, DATE_SERIALE, TXRDY, RXRDY);

	input[7:0] D;
	input CLK, RESET, START, RXRDY, DREQ;
	output DATE_SERIALE, TXRDY;

	wire SLI, SLI2, qa;
	wire incarca;
	wire DEPL;
	wire paritate;
	wire [3:0] q;
	
	x74_194 shift_1(
		.CLR(~RESET),
		.S1(incarca | DEPL),
		.S0(incarca),
		.SRI(),
		.SLI(SLI),
		.A(D[4]),
		.B(D[5]),
		.C(D[6]),
		.D(D[7]),
		.CK(CLK),
		.QA(SLI2),
		.QB(),
		.QC(),
		.QD()
	);

	x74_194 shift_1_2(
		.CLR(~RESET),
		.S1(incarca | DEPL),
		.S0(incarca),
		.SRI(),
		.SLI(SLI2),
		.A(D[0]),
		.B(D[1]),
		.C(D[2]),
		.D(D[3]),
		.CK(CLK),
		.QA(qa),
		.QB(),
		.QC(),
		.QD()
	);
	
	circuit_paritate H1(
		.date(D),
		.paritate(paritate)		
	);
	
	fdrse fdrse_1(
		.R(incarca & ~paritate),
		.S(incarca & paritate),
		.CE(DEPL),
		.D(1),
		.C(CLK),
		.Q(SLI)
	);
	
	fdrse fdrse_2(
		.R(incarca),
		.S(RESET),
		.CE(DEPL),
		.D(qa),
		.C(CLK),
		.Q(DATE_SERIALE)
	);
	
	cb4cle cb4cle(
		.CLR(RESET | TXRDY),
		.L(0),
		.CE(DEPL),
		.C(CLK),
		.D0(),
		.D1(),
		.D2(),
		.D3(),
		.Q(q),
		.TC(),
		.CEO()
	);

	H2 H2(
		.start(START),
		.reset(RESET),
		.rxrdy(0),
		.num12(~q[0] & ~q[1] & q[2] & q[3]),
		.dreq(0),
		.ceas(CLK),
		.txrdy(TXRDY),
		.rdy(),
		.depl(DEPL),
		.incarca(incarca),
		.ddisp()
	);
	
endmodule
