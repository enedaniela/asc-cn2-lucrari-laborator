`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:29:31 11/24/2016
// Design Name:   top
// Module Name:   T:/real_lucrare2/lucrare2/test.v
// Project Name:  lucrare2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test;

	// Inputs
	reg [7:0] D;
	reg CLK;
	reg RESET;
	reg START;
	reg RXRDY;
	reg DREQ;

	// Outputs
	wire DATE_SERIALE;
	wire TXRDY;

	// Instantiate the Unit Under Test (UUT)
	top uut (
		.D(D), 
		.CLK(CLK), 
		.RESET(RESET), 
		.START(START), 
		.DATE_SERIALE(DATE_SERIALE), 
		.TXRDY(TXRDY), 
		.RXRDY(RXRDY),
		.DREQ(DREQ)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		RESET = 1;
		START = 0;
		RXRDY = 0;
		DREQ = 0;
		
		D = 8'b10000111;
		
		#60
		RESET = 0;
		
		#40
		START = 1;
		RXRDY = 1;
		DREQ = 1;
		
		#150;	
		D = 8'b01010101;
		RESET = 1;
		START = 1;
		
		#20;
		RESET = 0;
		START = 0;
		
	end
	

	always #5 CLK = ~CLK;
		
endmodule

