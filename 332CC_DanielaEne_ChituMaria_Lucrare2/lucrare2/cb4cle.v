`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:44:50 11/24/2016 
// Design Name: 
// Module Name:    cb4cle 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cb4cle(CLR, L, CE, C, D0, D1, D2, D3, Q, TC, CEO);

	input CLR, L, CE, C, D0, D1, D2, D3;
	output reg TC, CEO;
	output reg[3:0] Q;

	always @ (posedge CLR or posedge C) begin
	
		if (CLR == 1) begin
			Q[0] = 0;
			Q[1] = 0;
			Q[2] = 0;
			Q[3] = 0;
			TC = 0;
			CEO = 0;
		end
		else begin
			if (L == 0) begin
					if (CE == 1) begin
						Q = Q + 1;
						TC = Q[3] * Q[2] * Q[1] * Q[0];
						CEO = TC * CE;
					end
			end
			else begin
				Q[3] = D3;
				Q[2] = D2;
				Q[1] = D1;
				Q[0] = D0;
				TC = Q[3] * Q[2] * Q[1] * Q[0];
				CEO = TC * CE;				
			end
		end
	end

endmodule
