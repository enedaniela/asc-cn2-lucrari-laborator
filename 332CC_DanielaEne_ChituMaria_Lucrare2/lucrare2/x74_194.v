`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:36:27 11/24/2016 
// Design Name: 
// Module Name:    x74_194 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module x74_194(CLR, S1, S0, SRI, SLI, A, B, C, D, CK, QA, QB, QC, QD);

	input CLR, S1, S0, SRI, SLI, A, B, C, D, CK;
	output reg QA, QB, QC, QD;
	
	always @ (posedge CK or negedge CLR) begin
	
		if (CLR == 0) begin
		
			QA = 0;
			QB = 0;
			QC = 0;
			QD = 0;
			
		end
		else begin
		
			if (S1 == 1) begin
				if (S0 == 1) begin
				
					QA = A;
					QB = B;
					QC = C;
					QD = D;
				
				end
				else if (S0 == 0) begin
					
					QA = QB;
					QB = QC;
					QC = QD;
					QD = SLI;
					
				end
			end
			else if (S0 == 1) begin		
			
					QD = QC;
					QC = QB;
					QB = QA;
					QA = SRI;
			
			end		
		end
	
	end

endmodule
