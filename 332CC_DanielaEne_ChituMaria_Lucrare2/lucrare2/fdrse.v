`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:57:10 11/17/2016 
// Design Name: 
// Module Name:    fdrse 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fdrse(R, S, CE, D, C, Q);

	input R, S, CE, D, C;
	output reg Q;
	
	initial begin
		Q = 0;
	end
	
	always @(posedge C) begin
		if (R == 1) begin
			Q = 0;
		end
		else begin
			if (S == 1) begin
				Q = 1;
			end
			else begin
				if (CE == 1) begin
					Q = D;
				end
				else begin
					Q = 0;
				end
			end
		end
	end


endmodule
